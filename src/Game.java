import java.util.Scanner;

public class Game {
private Player x;
private Player o;
private Board board;

public Game() {
	x = new Player('X');
	o = new Player('O');
	board  =new Board(x,o); 
}
public void play() {
	 showWelcome();
	 
		 showTable();
		 showTurn();
		 input();
		 
		  
	 
	 
}
private void showWelcome() {
	System.out.println("Welcome to OX Game");
}
private void showTable(){
	char [][] table = board.getTable();
	System.out.println("  1 2 3");
	for(int i =0 ;i<table.length;i++) {
		System.out.print(i+1);
		for(int j = 0 ;j<table[i].length;j++) {
			System.out.print(" "+ table[i][j]);
		}
		System.out.println();
	}	
}
public void showTurn() {
	Player player = board.getcurrentPlayer();
		System.out.println(player.getName()+"  turn");
}
private void input() {
	Scanner kb = new Scanner(System.in);
	while(true) {
		try {
			System.out.print("Please input Row Col");
			String input = kb.nextLine();
			String[] str = input.split(" ");
			if (str.length !=2) {
				System.out.println("Please input Row Col [1-3]");
				continue;
			}
			int row = Integer.parseInt(str[0]);
			int col = Integer.parseInt(str[1]);
			if(board.setTable(row,col) == false) {
				System.out.println("Table is not empty !!");
				continue;
			}
		}catch(Exception e) {
			System.out.println("Please input Row Col [1-3]");
			continue;
			
		}
		
	}

}
}
