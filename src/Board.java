
public class Board {
	private Player x ;
	private Player o ;
	private Player currentPlayer ;
	private char[][] table = {
			{'-', '-', '-'}, 
			{'-', '-', '-'},
			{'-', '-', '-'}
	};
public Board(Player x , Player o) {
	this.x = x;
	this.o = o;
	currentPlayer = x;
}
public char[][] getTable(){
	return table;
}
public Player getcurrentPlayer() {
	return currentPlayer;
}
public boolean setTable(int row , int col) {
	if (table[row][col] == '-') {
		table[row][col] = currentPlayer.getName();
		return true; 
		}
	return false;
	}
}
